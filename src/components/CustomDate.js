import React, { useEffect, useState } from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

const CustomDate = ({ getDate, handleBack, handleCancel }) => {
  const [startDate, setStartDate] = useState(null)
  const [endDate, setEndDate] = useState(null)
  const [isValid, setIsValid] = useState(false)

  useEffect(() => {
    if (startDate && endDate) {
      setIsValid(true)
    } else {
      setIsValid(false)
    }
  }, [startDate, endDate])

  const handleSaveClick = () => {
    if (!isValid) {
      return
    }
    getDate({ startDate, endDate })
  }

  return (
    <div className='datecustom'>
      <div className='datecustom__head'>
        <h2 className='title'>Date Range Picker</h2>
        <button className='btn btn-back' onClick={handleBack}>
          Back
        </button>
      </div>

      <div className='datecustom__content'>
        <DatePicker
          dateFormat='dd/MM/yyyy'
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          placeholderText='From date'
          selectsStart
          required
          minDate={new Date()}
          showDisabledMonthNavigation
          startDate={startDate}
          endDate={endDate}
          calendarClassName='datepicker-custom'
        />
        <DatePicker
          dateFormat='dd/MM/yyyy'
          selected={endDate}
          onChange={(date) => setEndDate(date)}
          placeholderText='To date'
          selectsEnd
          required
          startDate={startDate}
          showDisabledMonthNavigation
          endDate={endDate}
          minDate={startDate}
          calendarClassName='datepicker-custom'
        />
      </div>
      <div className='datecustom__footer'>
        <button className='btn btn-default' onClick={handleCancel}>
          Cancel
        </button>
        <button className='btn btn-primary' onClick={handleSaveClick} disabled={!isValid}>
          Save
        </button>
      </div>
    </div>
  )
}

export default CustomDate
