import React, { useState, useRef, useEffect } from 'react'
import CustomDate from './CustomDate'
import './Dropdown.scss'

const Icon = () => {
  return (
    <svg className='dropdown__icon' height='20' width='20' viewBox='0 0 20 20'>
      <path d='M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z'></path>
    </svg>
  )
}

const Dropdown = ({ label, variant, options, buttonClassName, addMoreItem }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [isShowDateCustom, setIsShowDateCustom] = useState(false)
  const [selectedOption, setSelectedOption] = useState()
  const [selectedDateRange, setSelectedDateRange] = useState(null)
  const dropdownRef = useRef(null)

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setIsOpen(false)
      }
    }
    window.addEventListener('click', handleOutsideClick)

    return () => {
      window.removeEventListener('click', handleOutsideClick)
    }
  }, [dropdownRef])

  const handleSelecBox = () => {
    setIsOpen(true)
    setIsShowDateCustom(false)
  }
  const handleSelect = (option) => {
    setSelectedOption(option)
    setSelectedDateRange()
  }

  const showCustomDate = () => {
    setIsShowDateCustom(true)
    setIsOpen(false)
  }

  const handleCancel = () => {
    setIsShowDateCustom(false)
  }

  const handleBack = () => {
    setIsShowDateCustom(false)
    setIsOpen(true)
  }

  const handleSaveDateRange = ({ startDate, endDate }) => {
    setSelectedDateRange(`${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`)
    setSelectedOption(selectedDateRange)
    setIsShowDateCustom(false)
  }

  const ItemDropdown = ({ option }) => {
    return (
      <div
        className={`dropdown__item ${selectedOption === option ? 'dropdown__item--active' : ''}`}
        onClick={() => handleSelect(option)}
      >
        {!variant && (
          <>
            <span className='label'>{option.title}</span>
            <span className='client-id'>{option.clienID}</span>
          </>
        )}

        {variant === 'radio' && (
          <>
            <span className='radio'></span>
            <span className='label'>{option.title}</span>
          </>
        )}

        {variant === 'image' && (
          <>
            <img src={option.image} alt={option.title} />
            <span className='label'>{option.title}</span>
          </>
        )}

        {variant === 'color' && (
          <>
            <span className='dropdown__color' style={{ backgroundColor: option.color }}></span>
            <span className='label'>{option.title}</span>
          </>
        )}
      </div>
    )
  }

  const dropdownClass = `dropdown ${isOpen || isShowDateCustom ? 'dropdown--open' : ''}`

  return (
    <div className={dropdownClass} ref={dropdownRef}>
      <label className='dropdown__label'>{label}</label>
      <div
        className={`dropdown__button ${
          selectedOption || selectedDateRange ? 'dropdown__button--active' : ''
        } ${buttonClassName}`}
        onClick={handleSelecBox}
      >
        {selectedDateRange ? (
          <div>{selectedDateRange}</div>
        ) : selectedOption ? (
          selectedOption !== null ? (
            <div className='dropdown__inner'>
              {variant === 'image' && (
                <img className='dropdown__image' src={selectedOption.image} alt={selectedOption.title} />
              )}
              {variant === 'color' && (
                <span className='dropdown__color' style={{ backgroundColor: selectedOption.color }}></span>
              )}
              {selectedOption.title}
            </div>
          ) : (
            'All'
          )
        ) : (
          'All'
        )}

        <Icon />
      </div>

      <div className={`dropdown__list ${!variant ? 'default' : ''} ${isOpen ? 'show' : 'hidden'}`}>
        {options.map((option, index) => (
          <>
            <ItemDropdown key={option.id} option={option} />
            {options.length === index + 1 && addMoreItem && (
              <div
                key={addMoreItem}
                className={`dropdown__item ${selectedOption === addMoreItem ? 'dropdown__item--active' : ''}`}
                onClick={() => showCustomDate(!isShowDateCustom)}
              >
                <span className='radio'></span>
                <span className='label'>{addMoreItem}</span>
              </div>
            )}
          </>
        ))}
      </div>

      <div className={`dropdown__list ${isShowDateCustom ? 'show' : 'hidden'}`}>
        <CustomDate
          getDate={handleSaveDateRange}
          handleBack={handleBack}
          handleCancel={handleCancel}
          onChange={setIsOpen}
        />
      </div>
    </div>
  )
}

export default Dropdown
