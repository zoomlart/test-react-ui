import React from 'react'
import { dataItemId, dataItemImage, dataItemRadio, dataItemTagColor } from '../data/data'
import Dropdown from './Dropdown'

const MainLayout = () => {
  return (
    <div className='form-group'>
      <Dropdown label='Client' options={dataItemId} />
      <Dropdown label='Date Range' variant='radio' options={dataItemRadio} addMoreItem='Custom' />
      <Dropdown label='Retoucher' variant='image' options={dataItemImage} />
      <Dropdown label='Status' variant='color' options={dataItemTagColor} />
    </div>
  )
}

export default MainLayout
