import React, { useEffect, useState } from 'react'
import { logoImage } from '../data/data'

const IconClose = () => {
  return (
    <svg
      className='sidebar__icon'
      xmlns='http://www.w3.org/2000/svg'
      width='14'
      height='14'
      fill='none'
      viewBox='0 0 14 14'
    >
      <path
        fill='#000'
        d='M13.707 1.707A1 1 0 0 0 12.293.293L7 5.586 1.707.293A1 1 0 0 0 .293 1.707L5.586 7 .293 12.293a1 1 0 1 0 1.414 1.414L7 8.414l5.293 5.293a1 1 0 0 0 1.414-1.414L8.414 7l5.293-5.293Z'
      />
    </svg>
  )
}

const Sidebar = (props) => {
  let { isMenuOpen, handleMenuClose } = props
  const [isShowMenu, setIsShowMenu] = useState(isMenuOpen)

  useEffect(() => {
    setIsShowMenu(isMenuOpen)
  }, [isMenuOpen])

  const handleCloseSidebar = () => {
    setIsShowMenu(false)
    handleMenuClose(false)
  }

  return (
    <div className={`sidebar ${isShowMenu ? 'sidebar--active' : ''}`}>
      <div className='sidebar__inner'>
        <div className='sidebar__logo'>
          <img src={logoImage[0].image} alt='Pixelz' />
          <span className='sidebar__close' onClick={handleCloseSidebar}>
            <IconClose />
          </span>
        </div>
        <div className='sidebar__menu'>
          <h2 className='sidebar__heading'>Default</h2>
        </div>
      </div>
    </div>
  )
}

export default Sidebar
