import React, { useEffect, useState } from 'react'
import './sass/main.scss'
import MainLayout from './components/MainLayout'
import Sidebar from './components/Sidebar'
import Navbar from './components/Navbar'

function App() {
  const [isMenuOpen, setIsMenuOpen] = useState(true)
  const [screenSize, setScreenSize] = useState(undefined)

  useEffect(() => {
    const handleResize = () => setScreenSize(window.innerWidth)

    window.addEventListener('resize', handleResize)

    handleResize()

    return () => window.removeEventListener('resize', handleResize)
  }, [])

  useEffect(() => {
    if (screenSize <= 900) {
      setIsMenuOpen(false)
    } else {
      setIsMenuOpen(true)
    }
  }, [screenSize])

  const handleMenuClose = () => {
    setIsMenuOpen(!isMenuOpen)
  }

  return (
    <>
      <div className='main'>
        <Sidebar isMenuOpen={isMenuOpen} handleMenuClose={handleMenuClose} />
        <div className='main__content'>
          <Navbar isMenuOpen={isMenuOpen} handleMenuClose={handleMenuClose} />
          <div className='main__wrapper'>
            <MainLayout />
          </div>
        </div>
      </div>
    </>
  )
}

export default App
