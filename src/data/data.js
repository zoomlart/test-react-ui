import avatar1 from './avatar1.png'
import avatar2 from './avatar2.png'
import avatar3 from './avatar3.png'
import avatar4 from './avatar4.png'
import avatar5 from './avatar5.png'
import logo from './logo.png'

export const logoImage = [
  {
    image: logo
  }
]

export const dataItemId = [
  {
    id: 1,
    title: 'Gwendoline Christie',
    clienID: '1147941'
  },
  {
    id: 2,
    title: 'RL Editorial',
    clienID: '1050694'
  },
  {
    id: 3,
    title: 'Nathalie Emmanuel',
    clienID: '1156984'
  },
  {
    id: 4,
    title: 'Vuori',
    clienID: '1177576'
  }
]

export const dataItemImage = [
  {
    id: 1,
    image: avatar1,
    title: 'Hanna Curtis'
  },
  {
    id: 2,
    image: avatar2,
    title: 'James Vetrovs'
  },
  {
    id: 3,
    image: avatar3,
    title: 'Jakob Kenter'
  },
  {
    id: 4,
    image: avatar4,
    title: 'Livia Korsgaard'
  },
  {
    id: 5,
    image: avatar5,
    title: 'Livia Korsgaard'
  }
]

export const dataItemTagColor = [
  {
    id: 1,
    color: '#FD853A',
    title: 'QA'
  },
  {
    id: 2,
    color: '#EC4A0A',
    title: 'Rejected'
  },
  {
    id: 3,
    color: '#C1453D',
    title: 'Expedited'
  }
]

export const dataItemRadio = [
  {
    id: 1,
    title: 'Today'
  },
  {
    id: 2,
    title: 'Yesterday'
  },
  {
    id: 3,
    title: 'This Week',
    default: true
  },
  {
    id: 4,
    title: 'Last Week'
  },
  {
    id: 5,
    title: 'This Month'
  },
  {
    id: 6,
    title: 'Last Month'
  }
]
